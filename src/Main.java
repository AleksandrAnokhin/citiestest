import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

public class Main {


    public static void main(String[] args) {
        performFirstTask(generateRandomArr(15));
        performSecondTask(generateRandomArr(15), generateRandomArr(5));
    }

    // Генератор входных данных
    public static int[] generateRandomArr(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = ThreadLocalRandom.current().nextInt(-1000, 1000);
        }
        return arr;
    }

    // Задание 2.1
    public static void performFirstTask(int[] input) {
        int minX = Arrays.stream(input).min().getAsInt();
        float sumY = 0;
        float[] arrY = new float[input.length];

        for (int i = 0; i < input.length; i++) {
            float y = (float) Math.pow(input[i], 3) / minX;
            arrY[i] = y;
            if (i % 2 != 0) sumY += y;
        }

        System.out.println(String.format("Min X %d", minX));
        System.out.println(String.format("Sum Y %f", sumY));
    }

    //Задание 2.2
    public static void performSecondTask(int[] inputA, int[] inputB) {
        ArrayList<Integer> arrayC = new ArrayList<>();

        for (int i = 0; i < inputA.length; i++) {
            if (inputA[i] < 0) arrayC.add(inputA[i]);
            if (i < inputB.length - 1 && inputB[i] < 0) arrayC.add(inputB[i]);
        }
        System.out.println(String.format("Array C is %s", arrayC.toString()));
    }

}
